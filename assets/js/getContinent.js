/**
 * Consulta dos diferentes API para obtener
 * el continenete de donde se conecta el cliente
 * @returns string
 */
export async function getContinent() {
    // Consultar la API ipify para obtener la IP del cliente
    const resp = await fetch('https://api.ipify.org?format=json');
    const { ip } = await resp.json();

    // Consulta a la api de geoPlugin para obtener la zona horaria del cliente
    // y posteriormente el continente
    // const geoPluginResp = await fetch(`http://geoplugin.net/json.gp?ip=${ip}`);
    const geoPluginResp = await fetch(`https://ipapi.co/${ip}/json/`);
    const { country_name, country_code } = await geoPluginResp.json();

    const restCountriesResp = await fetch(
        `https://restcountries.com/v2/alpha/${country_code}`
    );
    const { region: continente } = await restCountriesResp.json();

    return {
        continente,
        ip,
        country_name,
    };
}
