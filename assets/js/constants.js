export const API_SERVERS = {
    Americas: 'https://myapp-oal-02.azurewebsites.net',
    Europe: 'https://myapp-lrp-02.azurewebsites.net/',
    Asia: 'https://myapp-sjgc-02.azurewebsites.net/',
    Oceania: 'https://myapp-amvl-02.azurewebsites.net',
    Africa: 'https://myapp-macr-02.azurewebsites.net',
};
